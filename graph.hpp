/**
 * Lucio Messina (matricola 505134)
 * 
 * Final project: ACO-TSP.
 * 
 * graph.hpp - Declaration and documentation of functions that create, modify, use or destroy the graph.
 * For the implementation, see graph.cpp
 * 
 * */
 
#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <iostream>
#include <cmath>
#include <cstdio>
#include <utility>
#include <random>

#include "parser.hpp"

/**
 * 
 * Creates a new empty graph.
 * 
 * @param n number of nodes in the graph
 * @param fill value for all the distances in the graph (default 0).
 * 
 * @returns the lower triangle half of the graph distance matrix.
 *        	graph[i][j] (with i>j) is the distance from node i to node j.
 *          graph[i][j]=fill for each i>j.
 * 
 * */
float **new_empty_graph ( int n, float fill=0 );

/**
 * 
 * Destroys a graph, freeing memory.
 * 
 * @param graph graph to be destroyed
 * @param n number of nodes in the graph
 * 
 * */
void delete_graph ( float **graph, int n );

/**
 * 
 * prints a path to stdout or to a file.
 * 
 * @param path sorted sequence of nodes that compose the path
 * @param n number of nodes in the path
 * @param fout file on which the path will be written
 * 
 * */
void print_path ( float *path, int n, std::ofstream &fout);

/**
 * 
 * Simulates an ant moving trough the graph, starting from a random node and performing an hamiltonian path choosing the next city to visit according to:
 *  - the pheromone intensity on the links ( global_pheromone variable ); and
 *  - the distances between the cities ( global_visibilities variable ).
 * 
 * stores the path in the array referenced by the parameter path, and returns its the length.
 * 
 * @param path array in which the path is stored
 * 
 * @returns the lenght of the path
 * 
 * */
float simulate_travel ( float *path );

/**
 * 
 * converts a graph of distances into a graph of visibilities, according to the formula:
 * visibility[i][j] = ( 1 / distance[i][j] ) ^ beta
 * 
 * where distance[i][j] is the distance between node i and node j
 * beta is a parameter of the simulation to regulate the distance influence, read from the global_parameters variable
 * 
 * @param distances lower triangle half of the graph distance matrix.
 *        distances[i][j] (with i>j) is the distance from node i to node j.
 * 
 * @returns lower triangle half of the graph visibility matrix.
 *        	visibility[i][j] (with i>j) is the visibility from node i to node j.
 * 
 * */
float **distances_to_visibilities ( float **distances );

/**
 * 
 * measure the lenght of a path. The distance are read from the global_distances variable.
 * 
 * @param path sorted sequence of nodes that compose the path.
 * 
 * @returns the length of the path.
 * 
 * */
float measure_length ( float *path );

/**
 * 
 * Merges two graphs g1 and g2, both of them with n nodes. Only the lower half triangle of each graph is memorized:
 * g1[i][j] with i>j is the distance from node i to node j in graph g1.
 * g2[i][j] with i>j is the distance from node i to node j in graph g2.
 * 
 * For each link i,j the pheromone quantity on link i,j of graph g2 is added to the quantity on link i,j of g1.
 * Thus, g1 is modified by this function. It is also returned.
 *
 * At the end of this function all the links g1 are modified s.t. new_g1[i][j] = old_g1[i][j] + g2[i][j] for each i>j
 * 
 * @param g1 the "accumulator" graph.
 * @param g2 the other graph to be summed.
 * @param n number of nodes in the graph.
 * 
 * @returns a graph that represents the sum of g1 and g2.
 * 
 * */
float **accumulate_graphs ( float **g1, float **g2, int n );

/**
 * 
 * Update the global_pheromone graph, adding some new pheromone to its links.
 * The formula used for the update is:
 * 
 * new_global_pheromone[i][j] = global_pars.rho * old_global_pheromone[i][j] + pheromone_trail[i][j]
 * 
 * for each i>j, where global_pars.rho is a parameter of the simulation that regulates the pheromone evaporation and old_global_pheromone and new_global pheromone are the pheromone intensities before and after the update respectively.
 * 
 * @param pheromone_trail pheromone quantity to be added in the global_pheromone graph
 * 
 * */
void update_global_graph ( float **pheromone_trail );

/**
 * 
 * global variable that stores the visibilities among the nodes.
 * global_visibilities[i][j] (with i>j) is the visibility from node i to node j.
 * 
 * */
extern float **global_visibilities;

/**
 * 
 * global variable that stores the distances among the nodes.
 * global_distances[i][j] (with i>j) is the distance from node i to node j.
 * 
 * */
extern float **global_distances;

/**
 * 
 * global variable that stores the intensity of the pheromone trail on each link.
 * global_pheromone[i][j] (with i>j) is the intensity of pheromone on the link that connects node i to node j.
 * 
 * */
extern float **global_pheromone;

#endif
