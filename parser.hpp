/**
 * Lucio Messina (matricola 505134) 
 * 
 * Final project: ACO-TSP.
 * 
 * parser.hpp - Declaration and documentation of some parser utility for the command line arguments and the graph input files.
 * For the implementation, see parser.cpp
 * 
 * */
 
#ifndef PARSER_HPP
#define PARSER_HPP

#include <fstream>

/**
 * 
 * Structure that contains all the parameters of the simulation.
 * 
 *  - m: the maximum number of active threads (aka parallelism degree).
 *  - input_file: file from which the input graph is read.
 *  - time_performances: wheter performances have to be timed or not.
 *  - ants_number: number of ants traveling trough the graph.
 *  - max_iterations: number of iterations of the simulation.
 *  - alpha: coefficient to regulate the pheromone influence.
 *  - beta : coefficient to regulate the distances  influence.
 *  - rho  : coefficient to regulate the pheromone evapration.
 *  - Q    : coefficient to regulate the intensity of the pheromone trail produced by each ant.
 *  - n: number of nodes of the graph.
 * 
 * */
struct Parameters
{
	int m = 4;
	std::istream * input_file = NULL;
	bool time_performances = false;
	int ants_number = 20;
	int max_iterations = 100;
	float alpha = 1;
	float beta  = 1;
	float rho   = 0.9;
	float Q     = 1;
	int n = 0;
};

/**
 * 
 * prints the simulation parameters on stdout.
 * 
 * @param par the simulation parameters
 * 
 * */
void print_parameters ( Parameters par );

/**
 * 
 * prints the graph distances on stdout.
 * 
 * @param graph the lower triangle half of the graph distance matrix.
 *        graph[i][j] (with i>j) is the distance from node i to node j.
 * @param n the number of nodes in the graph
 * 
 * */
void print_graph ( float **graph, int n );

/**
 * 
 * parses the command line arguments and returns the parameters of the simulation.
 * For a list of all the possible command line arguments, exec `aco_seqential --help`
 * If an argument is missing, the default value is used.
 * 
 * @param argc number of command line arguments
 * @param argv the line arguments
 * 
 * @returns a Parameters structure with all the simulation parameters.
 * 
 * */
Parameters parse_command_line_arguments ( const int argc, const char**argv );

/**
 * 
 * parses an input file to read a graph.
 * all the lines starting with "#" will be ignored or used to extract trivial informations, such as the name of the graph.
 * all the lines with format "i<TAB>j<TAB>dist" will be used to extract the distance between node i to node j.
 *  i,j must be two integers, <TAB> is the tabulation character ("\t"), and dist is the distance value (floating point number).
 * 
 * the variable referenced by n will be valorized with the number of nodes in the graph.
 * 
 * @param input_file file from which the graph is read.
 * @param n reference to a variable which will be valorized with the number of nodes in the graph.
 * 
 * @returns the lower triangle half of the graph distance matrix.
 *        	graph[i][j] (with i>j) is the distance from node i to node j.
 * 
 * */
float **read_graph ( std::istream &input_file, int &n );

/**
 * 
 * global variable to memorize the simulation parameters.
 * 
 * */
extern Parameters global_pars;

#endif
