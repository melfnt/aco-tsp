/**
 * Lucio Messina (matricola 505134) 
 * 
 * Final project: ACO-TSP.
 * 
 * aco_fastflow.cpp - fast flow version.
 * Contains the main function for the fast flow version.
 * 
 * */
 
#include "parser.hpp"
#include "graph.hpp"

#include <iostream>
#include <fstream>
#include <limits>

#include <ff/pipeline.hpp>
#include <ff/farm.hpp>
using namespace ff;

#define _MINIBATCH_SIZE 10

struct Master: ff_node_t<float**, int> 
{
	Master ( int ants_number )
	{
		this->s = ants_number;
		this->iteration_number = 0;
		this->total_minibatches = s/_MINIBATCH_SIZE + ( s%_MINIBATCH_SIZE != 0 ? 1 : 0 );
	}
	
	void distribute_ants ()
	{
		for (int i=0; i<this->s/_MINIBATCH_SIZE; ++i) 
		{ 
			//~ std::cout << "[MASTER] distributed "<<_MINIBATCH_SIZE<<" ants\n";
			ff_send_out ( (int*) _MINIBATCH_SIZE  ); 
		}
		if ( s%_MINIBATCH_SIZE != 0 )
		{
			//~ std::cout << "[MASTER] distributed "<<s%_MINIBATCH_SIZE<<" ants\n";
			ff_send_out ( (int*) ((long)s%_MINIBATCH_SIZE) );
		}
	}
	
	int* svc (float ***task) 
	{
		if ( task == nullptr )
		{
			//~ std::cout << "[MASTER] distributing ants\n";
			this->distribute_ants ();
			this->pheromone_trail = new_empty_graph ( global_pars.n );
			this->trails_received = 0;
			//~ std::cout << "[MASTER] finished distributing ants\n";
			return GO_ON;            
		}
		
		//~ std::cout << "[MASTER] pheromone trail received\n";
		float **t = *task;
		accumulate_graphs ( this->pheromone_trail, t, global_pars.n );
		delete_graph (t, global_pars.n);
		delete [] task;
		
		++this->trails_received;
		//~ std::cout << "[MASTER] pheromone trail added. It was number "<<this->trails_received<<"\n";
		
		if ( this->trails_received == this->total_minibatches )
		{
			//~ std::cout << "[MASTER] It was the last one. It's time to update the graph...\n";
			update_global_graph ( pheromone_trail );
			//~ std::cout << "[MASTER] Done. Now it's time for another iteration!\n";
			++ this->iteration_number;
			if ( this->iteration_number >= global_pars.max_iterations )
			{
				//~ std::cout << "[MASTER] It was the last iteration!\n";
				return EOS;
			}
			delete [] pheromone_trail;
			this->distribute_ants ();
			this->pheromone_trail = new_empty_graph ( global_pars.n );
			this->trails_received = 0;
			
		}
		return GO_ON;         
	}
 
	int s;
	int iteration_number;
	int total_minibatches;
	float **pheromone_trail;
	int trails_received;
};

struct Slave: ff_node_t<int, float**> 
{
	Slave ( float **shortest_path_ref, float *shortest_path_length_ref )
	{
		path = new float[global_pars.n];
		this->shortest_path_ref = shortest_path_ref;
		this->shortest_path_length_ref = shortest_path_length_ref;
	}

	float *** svc ( int *task ) 
	{
		int s = (int)(long)task;
		float length;
		int n = global_pars.n;
		float ***ret = new float**;
		*ret = new_empty_graph (n);
		float **pheromone_trail = *ret;
		//~ std::cout << "[SLAVE] doing job with "<<s<<" ants\n";
		for ( int k=0; k<s; ++k )
		{
			int x,y;
			length = simulate_travel ( path );
			for ( int i=0; i<n-1; ++i )
			{
				x = std::min ( path[i], path[i+1] );
				y = std::max ( path[i], path[i+1] );
				pheromone_trail[y][x] += global_pars.Q / length;
			}
			x = std::min ( path[n-1], path[0] );
			y = std::max ( path[n-1], path[0] );
			pheromone_trail[y][x] += global_pars.Q / length;
			if ( length < min_length )
			{
				min_length = length;
				delete[] min_length_path;
				min_length_path = path;
				path = new float[n];
			}
		}
		return ret;
	}
 
	void svc_end() 
	{
		//~ std::cout << "[SLAVE] finalizing... min length found: "<<min_length<<"\n";
		delete [] path;
		*shortest_path_ref = min_length_path;
		*shortest_path_length_ref = min_length;
	}
	
	float *min_length_path = NULL;
	float *path = NULL;
	float min_length = std::numeric_limits<float>::infinity();
	float **shortest_path_ref;
	float *shortest_path_length_ref;
};

int main ( const int argc, const char **argv )
{
	global_pars = parse_command_line_arguments ( argc, argv ) ;
	global_distances = read_graph ( *global_pars.input_file, global_pars.n );
	global_visibilities = distances_to_visibilities ( global_distances );
	int m = global_pars.m-1, n=global_pars.n;
	global_pheromone = new_empty_graph ( n );
	
	float **workers_min_paths = new float* [m];
	float *workers_paths_lengths = new float [m];
	
	Master master (global_pars.ants_number);
	std::vector<std::unique_ptr<ff_node> > W;
	for(int i=0;i<m;++i)
	{
		W.push_back(make_unique<Slave>( &(workers_min_paths[i]), &(workers_paths_lengths[i]) ));
	}
	
	ff_Farm<int> farm(std::move(W), master);
	farm.remove_collector();
	farm.wrap_around();
	farm.set_scheduling_ondemand();
	farm.run_and_wait_end();
	
	float min_length = std::numeric_limits<float>::infinity();;
	float *min_path = NULL;
	
	for ( int i=0; i<m; ++i )
	{
		if ( workers_paths_lengths[i] < min_length )
		{
			min_length = workers_paths_lengths[i];
			delete [] min_path;
			min_path = workers_min_paths[i];
		}
		else
		{
			delete [] workers_min_paths[i];
		}
	}
		
	std::ofstream fout;
	fout.open ("tests_results/best_path_found");
	print_path ( min_path, n, fout );
	
	delete [] min_path;
	delete [] workers_min_paths;
	delete [] workers_paths_lengths;
	
	delete_graph ( global_pheromone, n );
	delete_graph ( global_distances, n );
	delete_graph ( global_visibilities, n );
	
	return 0;
}
