/**
 * Lucio Messina (matricola 505134)
 * 
 * Final project: ACO-TSP.
 * 
 * Chronometer.cpp - documentation of a class used to measure times.
 * For the implementation, see Chronometer.cpp
 * 
 * */

#ifndef _CHRONOMETER_HPP
#define _CHRONOMETER_HPP

#include <chrono>
#include <string>
#include <fstream>

/**
 * 
 * Chronometer used to measure times.
 * Allows measuring times elapsed between two instant and records all the measurements on a report file.
 * 
 * The report lines have the format "NAME<TAB>TIME" where NAME is the name of the measure, <TAB> is the tabulation character ("\t") and TIME is the time elapsed.
 * 
 * */
class Chronometer
{
	public:
		/**
		 * 
		 * Creates a new chronometer.
		 * 
		 * \param report_fname name of the file on which the measuraments will be saved
		 * 
		 * */
		Chronometer ( std::string report_fname="tests_results/performances.tsv" );
		
		/**
		 * 
		 * Measure the time elapsed between this chronometer creation or the last call to lap() or restart() and the current moment.
		 * 
		 * \param name name of the measurament to be written in the report.
		 * 
		 * */
		void lap (  std::string name="lap" );
		
		/**
		 * 
		 * restarts the chronometer.
		 * The next time will be measured from this moment.
		 * 
		 * */
		void restart ();
	
	private:
		/**
		 * 
		 * last measured time
		 * 
		 * */
		std::chrono::time_point<std::chrono::high_resolution_clock> started_time;
		/**
		 * 
		 * file used to write the report
		 * 
		 * */
		std::ofstream fout;
};

#endif

