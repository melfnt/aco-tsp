/**
 * Lucio Messina (matricola 505134) 
 * 
 * Final project: ACO-TSP.
 * 
 * synchronization.hpp - Declaration and documentation of the functions ad data structures used to synchronize threads.
 * For the implementation, see synchronization.cpp
 * 
 * */

#ifndef SYNCHRONIZATION_HPP
#define SYNCHRONIZATION_HPP

#include <condition_variable>
#include <mutex>

#include "parser.hpp"
#include "graph.hpp"
#include "Chronometer.hpp"

/**
 * 
 * Global chronometer, usable by all the threads to measure times.
 * 
 * */
extern Chronometer global_ch;

/**
 * 
 * Structure used to implement parallel merging of multiple pheromone trails produced by different workers.
 * Two or more trails can be merged summing up the pheromone quantity on each link. Let m be the number of trails to merge:
 *  + At the beginning,   m/2 workers merge 2 trails each, resulting in m/2 trails;
 *  + In the second step, m/4 workers merge 2 trails each, resulting in m/4 trails;
 *  + In the third step,  m/8 workers merge 2 trails each, resulting in m/8 trails;
 * ...
 * And so on, until only one trail remains: this trail is the result of merging all the m initial trails.
 * Using this strategy, the maximum achievable parallelism degree is m/2 (in the first step); furthermore, m trails can be merged using only log(m) sequential steps.
 * 
 * The field of this data structure are:
 * - trail: one trail to be merged. If trail is NULL, no trails are available to be merged.
 * - finished_travels: number of workers that finished the travel part. Used only to measure performances
 * - size:  the size of the trail to be merged. If size==m, the merging process is finished.
 * - mtx, cv: mutex and condition variable to implement mutual exclusion 
 * 
 * */
struct TrailToBeMergedConcurrently
{
	float **trail = NULL;
	int size=0;
	int finished_travels=0;
	std::mutex mtx;
	std::condition_variable cv;
};

/**
 * 
 * Merges a pheromone trail produced by a single worker with other trails produced by the other workers, then updates the graph.
 * Then continues merging the resulting trail with the resulting trail of other merging steps, until there are no more trails to merge.
 * In order to merge the pheromone trails, a TrailToBeMergedConcurrently structure tm must be shared among all the parallel workers.
 * 
 * procedure executed by worker w to merge a trail T produced by the worker itself, with the trails produced by other workers:
 *     if no other trails (produced by another worker) is ready to be merged in tm:
 *         put T in tm as first trail to be merged
 *         the merging procedure is finished, this thread has nothing more to do
 *     else if another trail T' is ready to be merged in tm:
 *         remove T' from tm
 *         T = merge ( T, T' )
 *         repeat the whole procedure from scratch
 * 
 * The above procedure is executed in parallel by all the workers, until the size of the task of the last trail merged is equal to m.
 * Then, the worker that merged the last two trails updates the global_pheromone graph.
 * 
 * @param T initial trail to be merged
 * @param tm TrailToBeMergedConcurrently structure, shared among all the parallel workers
 * 
 * */
void synchronize_for_trail_merging_and_updating ( float **T, TrailToBeMergedConcurrently *tm );

#endif
