/**
 * Lucio Messina (matricola 505134) 
 * 
 * Final project: ACO-TSP.
 * 
 * synchronization.cpp - implementation of the functions ad data structures used to synchronize threads.
 * For the documentation, see synchronization.hpp
 * 
 * */

#include "synchronization.hpp"

Chronometer global_ch;

void synchronize_for_trail_merging_and_updating ( float **T, TrailToBeMergedConcurrently *tm )
{
	bool finished =  false;
	int size_of_T = 1, n=global_pars.n;
	std::unique_lock<std::mutex> lck (tm->mtx);
	++ tm->finished_travels;
	if ( tm->finished_travels == global_pars.m )
	{
		if (global_pars.time_performances) global_ch.lap ("T1");
		tm->finished_travels = 0;
	}
	lck.unlock ();
	while ( ! finished )
	{
		lck.lock ();
		if ( size_of_T == global_pars.m )
		{
			tm->trail = NULL;
			if (global_pars.time_performances) global_ch.lap ("T2");
			update_global_graph ( T );
			if (global_pars.time_performances) global_ch.lap ("T3");
			tm->cv.notify_all ();
			lck.unlock ();
			delete_graph (T, n);
			finished = true;
		}
		else if ( tm->trail == NULL )
		{
			tm->trail = T;
			tm->size = size_of_T;
			tm->cv.wait ( lck );
			lck.unlock ();
			finished = true;
		}
		else
		{
			float **other_trail = tm->trail;
			int other_size = tm->size;
			tm->trail = NULL;
			lck.unlock ();
			accumulate_graphs ( T, other_trail, n );
			delete_graph ( other_trail, n );
			size_of_T += other_size;
		}
	}
}
