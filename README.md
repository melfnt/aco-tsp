# aco-tsp 
implements ACO (Ant Colony Optimization) to solve TSP (Travel Salesman Problem).

written by Lucio Messina (matricola 505134)

USAGE

	./aco_sequential [OPTIONS]
	./aco_parallel [OPTIONS]
	./aco_fastflow [OPTIONS]

OPTIONS:
	
	-h --help
	    displays this help message and exits
	-m --parallelism-degree m
	    specifies the parallelism degree (default 4)
	-I --iterations-number I
	    specifies the iterations number (default 100)
	-s --ants-number s
	    specifies the ants number (default 20)
	-f --input-file FILE
	    specifies the input file (default: read stdin)
	-t --time-performances
	    if set, the performances are measured
	-a --alpha a
	    regulates the pheromone influence
	-b --beta b
	    regulates the distances influence
	-p --rho p
	   regulates the pheromone evaporation
	-Q --pheromone-quantity Q
	    regulates the intensity of the pheromone trail produced by each ant
	

INPUT FILE FORMAT

all the lines starting with "#" will be ignored or used to extract trivial information, such as the name of the graph.

all the lines with format "i<TAB>j<TAB>dist" will be used to extract the distance between node i to node j.

i,j must be two integers, <TAB> is the tabulation character ("\t"), and dist is the distance value (floating point number).

