/**
 * Lucio Messina (matricola 505134) 
 * 
 * Final project: ACO-TSP.
 * 
 * parser.cpp - Implementaton of some parser utility for the command line arguments and the graph input files.
 * For the documentaion, see parser.hpp
 * 
 * */
 
#include "parser.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

Parameters global_pars;

void print_parameters ( Parameters par )
{
	std::cout << "PARAMETERS:" << "\n";
	std::cout << "parallelism degree: " << par.m << "\n";
	std::cout << "time performances: " << par.time_performances << "\n";
	std::cout << "ants number: " << par.ants_number << "\n";
	std::cout << "iterations number: " << par.max_iterations << "\n";
	std::cout << "alpha: " << par.alpha << " beta: " << par.beta << "\n";
	std::cout << "rho: " << par.rho << " Q: " << par.Q << "\n";
}

const char * check_and_get_next_argument ( const char *option, int &c, const int argc, const char **argv )
{
	if ( c==argc-1)
	{
		std::cerr << "Needed an argument for option "<<option<<"\n";
		exit (-1);
	}
	++ c;
	return argv[c];
}

void usage ( const char *program_name )
{
	std::cout << "aco-tsp" << " - implements ACO to solve TSP\n";
	std::cout << "written by Lucio Messina (matricola 505134)\n\n";
	
	std::cout << "USAGE" << "\n";
	std::cout << program_name << " [OPTIONS]\n\n";
	
	std::cout << "possible options:\n";
	std::cout << "-h --help\n";
	std::cout << "    displays this help message and exits\n";
	std::cout << "-m --parallelism-degree m\n";
	std::cout << "    specifies the parallelism degree (default 4)\n";
	std::cout << "-I --iterations-number I\n";
	std::cout << "    specifies the iterations number (default 100)\n";
	std::cout << "-s --ants-number s\n";
	std::cout << "    specifies the ants number (default 20)\n";
	std::cout << "-f --input-file FILE\n";
	std::cout << "    specifies the input file (default: read stdin)\n";
	std::cout << "-t --time-performances\n";
	std::cout << "    if set, the performances are measured\n";
	std::cout << "-a --alpha a\n";
	std::cout << "    regulates the pheromone influence\n";
	std::cout << "-b --beta b\n";
	std::cout << "    regulates the distances influence\n";
	std::cout << "-p --rho p\n";
	std::cout << "    regulates the pheromone evaporation\n";
	std::cout << "-Q --pheromone-quantity Q\n";
	std::cout << "    regulates the intensity of the pheromone trail produced by each ant\n";
	
	std::cout << "\nInput file format\n";
	std::cout << "all the lines starting with \"#\" will be ignored or used to extract trivial information, such as the name of the graph.\n";
	std::cout << "all the lines with format \"i<TAB>j<TAB>dist\" will be used to extract the distance between node i to node j.\n";
	std::cout << "i,j must be two integers, <TAB> is the tabulation character (\"\\t\"), and dist is the distance value (floating point number).\n\n";
	
}

Parameters parse_command_line_arguments ( const int argc, const char**argv )
{
	Parameters ret;
	int c=1;
	std::string argument;
	ret.input_file = &std::cin;
	while ( c<argc )
	{
		//~ std::cout << "Argument "<<argv[c]<<"\n";
		if ( ! strcmp (argv[c], "-h") || ! strcmp (argv[c], "--help") )
		{
			usage ( argv[0] );
			exit (1);
		}
		else if ( ! strcmp (argv[c], "-m") || ! strcmp (argv[c], "--parallelism-degree") )
		{
			argument = check_and_get_next_argument ( argv[c], c, argc, argv );
			ret.m = ::atoi(argument.c_str());
		}
		else if ( ! strcmp (argv[c], "-s") || ! strcmp (argv[c], "--ants-number") )
		{
			argument = check_and_get_next_argument ( argv[c], c, argc, argv );
			ret.ants_number = ::atoi(argument.c_str());
		}
		else if ( ! strcmp (argv[c], "-f") || ! strcmp (argv[c], "--input-file") )
		{
			argument = check_and_get_next_argument ( argv[c], c, argc, argv );
			ret.input_file = new std::ifstream ( argument );
		}
		else if ( ! strcmp (argv[c], "-t") || ! strcmp (argv[c], "--time-performances") )
		{
			ret.time_performances = true;
		}
		else if ( ! strcmp (argv[c], "-a") || ! strcmp (argv[c], "--alpha") )
		{
			argument = check_and_get_next_argument ( argv[c], c, argc, argv );
			ret.alpha = ::atof ( argument.c_str() );
		}
		else if ( ! strcmp (argv[c], "-b") || ! strcmp (argv[c], "--beta") )
		{
			argument = check_and_get_next_argument ( argv[c], c, argc, argv );
			ret.beta = ::atof ( argument.c_str() );
		}
		else if ( ! strcmp (argv[c], "-p") || ! strcmp (argv[c], "--rho") )
		{
			argument = check_and_get_next_argument ( argv[c], c, argc, argv );
			ret.rho = ::atof ( argument.c_str() );
		}
		else if ( ! strcmp (argv[c], "-Q") || ! strcmp (argv[c], "--pheromone-quantity") )
		{
			argument = check_and_get_next_argument ( argv[c], c, argc, argv );
			ret.Q = ::atoi ( argument.c_str() );
		}
		else if ( ! strcmp (argv[c], "-I") || ! strcmp (argv[c], "--iterations-number") )
		{
			argument = check_and_get_next_argument ( argv[c], c, argc, argv );
			ret.max_iterations = ::atoi ( argument.c_str() );
		}
		else
		{
			std::cerr << "Unrecognised option: "<<argv[c]<<"\n";
			usage ( argv[0] );
			exit(-1);
		}
		++c;
	}
	return ret;
}

float **read_graph ( std::istream &input_file, int &n )
{
	std::string line;
	int i,j;
	float dist;
	float **ret;
	std::getline (input_file, line);
	std::getline (input_file, line);
	std::getline (input_file, line);
	n = ::atoi (line.substr (14).c_str());
	std::getline (input_file, line);
	std::getline (input_file, line);
	ret = new float* [n];
	ret[0] = NULL;
	for ( int c=1; c<n; ++c )
	{
		ret[c] = new float[c];
	}
	while ( line != "" )
	{
		//~ std::cout << "line: "<<line<<"\n";
		sscanf (line.c_str(), "%d %d %f", &i, &j, &dist);
		//~ std::cout << "i:"<<i<<" j:"<<j<<" dist:"<<dist<<"\n";
		ret[j][i] = dist;
		std::getline (input_file, line);
	}
	return ret;
}

void print_graph ( float **graph, int n )
{
	std::cout << "graph with n:" << n <<" nodes:\n";
	for (int i=1; i<n; ++i)
	{
		for (int j=0; j<i; ++j)
		{
			std::cout<<graph[i][j]<<"\t";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}
