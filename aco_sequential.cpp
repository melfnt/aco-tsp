/**
 * Lucio Messina (matricola 505134) 
 * 
 * Final project: ACO-TSP.
 * 
 * aco_sequential.cpp - reference sequential version.
 * Contains the main function for the reference sequential version.
 * 
 * */
 
#include "parser.hpp"
#include "graph.hpp"
#include "Chronometer.hpp"

#include <iostream>
#include <fstream>
#include <limits>

/**
 * 
 * Executes the ACO algorithm in a sequential fashon:
 * 
 * parse the command line argument
 * for i=0 to iterations number:
 *     for k=0 to ant number:
 *         simulate travel for a single ant
 *     update the pheromone trail
 * output the shortest path found
 * 
 * */
int main ( const int argc, const char **argv )
{
	Chronometer ch;
	global_pars = parse_command_line_arguments ( argc, argv ) ;
	//~ print_parameters ( global_pars );
	global_distances = read_graph ( *global_pars.input_file, global_pars.n );
	global_visibilities = distances_to_visibilities ( global_distances );
	int n = global_pars.n;
	//~ print_graph ( G, global_pars.n );
	float *path = new float[n];
	float *min_length_path = NULL;
	float length;
	float min_length = std::numeric_limits<float>::infinity();
	global_pheromone = new_empty_graph ( n );
	float **pheromone_trail;
	if (global_pars.time_performances) ch.lap ( "init" );
	for ( int c=0; c<global_pars.max_iterations; ++c )
	{
		//~ std::cout << "ITERATION "<<c<<"\n";
		pheromone_trail = new_empty_graph ( n );
		for ( int k=0; k<global_pars.ants_number; ++k )
		{
			int x,y;
			if (global_pars.time_performances) ch.restart();
			length = simulate_travel ( path );
			//~ std::cout << "ant" << k << " path:\n";
			//~ print_path ( path, n );
			//~ std::cin >> x;
			for ( int i=0; i<n-1; ++i )
			{
				x = std::min ( path[i], path[i+1] );
				y = std::max ( path[i], path[i+1] );
				pheromone_trail[y][x] += global_pars.Q / length;
			}
			x = std::min ( path[n-1], path[0] );
			y = std::max ( path[n-1], path[0] );
			pheromone_trail[y][x] += global_pars.Q / length;
			if ( length < min_length )
			{
				min_length = length;
				delete[] min_length_path;
				min_length_path = path;
				path = new float[n];
			}
			if (global_pars.time_performances) ch.lap ( "travel" );
		}
		update_global_graph ( pheromone_trail );
		delete_graph ( pheromone_trail, n );
		if (global_pars.time_performances) ch.lap ( "update" );
	}
	
	std::ofstream fout;
	fout.open ("tests_results/best_path_found");
	
	print_path ( min_length_path, n, fout );
	
	delete[] path;
	delete[] min_length_path;
	delete_graph ( global_pheromone, n );
	delete_graph ( global_distances, n );
	delete_graph ( global_visibilities, n );
	
	if (global_pars.time_performances) ch.lap ( "fin" );
	
	return 0;
}
