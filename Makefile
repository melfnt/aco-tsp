
CPPFLAGS=-std=c++11 -g -Wall -pedantic -pthread
ifndef FF_ROOT 
FF_ROOT = .
endif

parser.o: parser.cpp parser.hpp

graph.o: graph.cpp graph.hpp parser.hpp

Chronometer.o: Chronometer.hpp Chronometer.cpp

synchronization.o: synchronization.cpp synchronization.hpp parser.hpp graph.hpp Chronometer.hpp


aco_sequential: aco_sequential.cpp parser.o graph.o	Chronometer.o

aco_parallel: aco_parallel.cpp parser.o graph.o synchronization.o Chronometer.o

aco_fastflow: aco_fastflow.cpp parser.o graph.o
	g++ $(CPPFLAGS) -I $(FF_ROOT) -o $@ $^

clean:
	-rm -f *.o aco_sequential aco_parallel aco_fastflow

all: aco_parallel aco_sequential aco_fastflow

test: all
	mkdir -p tests_results
	python3 utilities/run_tests.py

remote_test:
	python3 utilities/run_remote_tests.py
	
.PHONY: clean all test remote_test
