/**
 * Lucio Messina (matricola 505134)
 * 
 * Final project: ACO-TSP.
 * 
 * graph.cpp - Implementaton of functions that create, modify, use or destroy the graph.
 * For the documentaion, see graph.hpp
 * 
 * */
 
#include "graph.hpp"

float **global_visibilities;
float **global_distances;
float **global_pheromone;

float **new_empty_graph ( int n, float fill )
{
	float **ret = new float* [n];
	ret[0] = NULL;
	for ( int i=1; i<n; ++i )
	{
		ret[i] = new float[i];
		for ( int j=0; j<i; ++j )
		{
			ret[i][j] = fill;
		}
	}
	return ret;
}

void delete_graph ( float **graph, int n )
{
	for ( int i=1; i<n; ++i )
	{
		delete[] graph[i];
	}
	delete[] graph;
}

void print_path ( float *path, int n, std::ofstream &fout )
{
	float length = measure_length ( path );
	fout << "path length: " << length << "\n";
	for ( int i=0; i<n; ++i )
	{
		fout << path[i] << "\n";
	}
	fout << "-1\nEOF\n";
}

float simulate_travel ( float *path )
{
	int n = global_pars.n;
	int i,j;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> random_generator(0, 1);
	bool *visited = new bool[n];
	for ( int c=0; c<n; ++c )
	{
		visited[c] = false;
	}
	i = random ()%n;
	path[0] = i;
	visited[i] = true;
	//~ std::cout<< "[THREAD"<<std::this_thread::get_id()<<"] first city " << i << "\n";
	for ( int c=1; c<n; ++c )
	{
		//~ std::cout<< "[THREAD"<<std::this_thread::get_id()<<"] city " << c << "\n";
		//~ std::cout<< "[THREAD"<<std::this_thread::get_id()<<"] probabilities:\n";
		float r,den = 0;
		int last_unvisited = 0;
		for ( j=0; j<n; ++j )
		{
			if ( ! visited[j] ) 
			{
				int x = std::min (i,j);
				int y = std::max (i,j);
				last_unvisited = j;
				//~ std::cout << "[THREAD"<<std::this_thread::get_id()<<"] j "<<j<< " pheromone " << global_pheromone[y][x] << " visibility " << global_visibilities[y][x]<<"\n";
				//~ std::cout << "[THREAD"<<std::this_thread::get_id()<<"] j "<<j<< " prob(numerator) " << pow ( global_pheromone[y][x], global_pars.alpha ) * global_visibilities[y][x]<<"\n";
				if (global_pheromone[y][x])
				{
					den += pow ( global_pheromone[y][x], global_pars.alpha ) * global_visibilities[y][x];
				}
				else
				{
					den += global_visibilities[y][x];
				}
			}
		}
		//~ std::cout<< "[THREAD"<<std::this_thread::get_id()<<"] denominator: "<<den<<"\n";
		r = random_generator (gen);
		//~ std::cout<< "\n[THREAD"<<std::this_thread::get_id()<<"]" << "random number: "<<r<<"\n";
		j = 0;
		while ( r>0 && j<=last_unvisited )
		{
			if ( ! visited[j] )
			{
				int x = std::min (i,j);
				int y = std::max (i,j);
				r -= pow ( global_pheromone[y][x], global_pars.alpha ) * global_visibilities[y][x] / den;
			}
			//~ std::cout<< "[THREAD"<<std::this_thread::get_id()<<"] j:" <<j<<" r:"<<r<<"\n";
			++j;
		}
		--j;
		//~ std::cout<< "[THREAD"<<std::this_thread::get_id()<<"] choosen city "<<c<<": "<<j<<"\n";
		path[c] = j;
		visited[j] = true;
		i = j;
	}
	delete [] visited;
	return measure_length ( path );
}

float **distances_to_visibilities ( float **distances )
{
	int n = global_pars.n;
	float **visibilities = new_empty_graph ( n );
	for ( int i=1; i<n; ++i )
	{
		for ( int j=0; j<i; ++j )
		{
			visibilities[i][j] = 1/pow(distances[i][j], global_pars.beta);
		}
	}
	return visibilities;
}

float measure_length ( float *path )
{
	int n = global_pars.n;
	float total_length = 0;
	for ( int i=0; i<n-1; ++i )
	{
		int x = std::min (path[i],path[i+1]);
		int y = std::max (path[i],path[i+1]);
		total_length += global_distances[y][x];
	}
	int x = std::min (path[0],path[n-1]);
	int y = std::max (path[0],path[n-1]);
	total_length += global_distances[y][x];
	return total_length;
}

float **accumulate_graphs ( float **g1, float **g2, int n )
{
	for ( int i=1; i<n; ++i )
	{
		for ( int j=0; j<i; ++j )
		{
			g1[i][j] = g1[i][j] + g2[i][j];
		}
	}
	return g1;
}

void update_global_graph ( float **pheromone_trail )
{
	for ( int i=1; i<global_pars.n; ++i )
	{
		for ( int j=0; j<i; ++j )
		{
			global_pheromone[i][j] = global_pars.rho * global_pheromone[i][j] + pheromone_trail[i][j];
		}
	}
}
