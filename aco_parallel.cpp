/**
 * Lucio Messina (matricola 505134) 
 * 
 * Final project: ACO-TSP.
 * 
 * aco_parallel.cpp - implementation of the functions used in the parallel version of the application.
 * 
 * */

#include <iostream>
#include <thread>
#include <vector>

#include "synchronization.hpp"
#include "parser.hpp"
#include "graph.hpp"
#include "Chronometer.hpp"

/**
 * 
 * routine executed in parallel by each worker:
 *     for i=0 to iterations number:
 *         for k=0 to ants number:
 *             p = simulate travel for a single ant
 *         merge the pheromone trails produced by all the workers in parallel
 *         update the pheromone quantity on the graph
 * 
 * \param ants_number number of ants that have to be moved by this worker.
 * \param tm structure to synchronize workers when merging the pheromone trails.
 * \param min_path_ref pointer to a location in which the shortest path found by this worker will be saved.
 * \param min_lenfth_ref pointer to a location in which the length of the shortest path found by this worker will be saved.
 * 
 * */
void worker_routine ( int ants_number, TrailToBeMergedConcurrently *tm, float **min_path_ref, float *min_length_ref )
{
	int n = global_pars.n;
	float *path = new float[n];
	float *min_length_path = NULL;
	float length;
	float min_length = std::numeric_limits<float>::infinity();
	float **pheromone_trail;
	//~ std::cout << "[THREAD"<<std::this_thread::get_id()<<"] Initialized. Ants number:"<<ants_number<<"\n";
	for ( int c=0; c<global_pars.max_iterations; ++c )
	{
		//~ std::cout << "[THREAD"<<std::this_thread::get_id()<<"] Iteration "<<c<<"\n";
		pheromone_trail = new_empty_graph ( n );
		for ( int k=0; k<ants_number; ++k )
		{
			int x,y;
			//~ std::cout << "[THREAD"<<std::this_thread::get_id()<<"] iter "<<c<<" ant "<<k<<"\n";
			length = simulate_travel ( path );
			//~ std::cout << "[THREAD"<<std::this_thread::get_id()<<"] length "<<length<<"\n";
			for ( int i=0; i<n-1; ++i )
			{
				x = std::min ( path[i], path[i+1] );
				y = std::max ( path[i], path[i+1] );
				pheromone_trail[y][x] += global_pars.Q / length;
			}
			x = std::min ( path[n-1], path[0] );
			y = std::max ( path[n-1], path[0] );
			pheromone_trail[y][x] += global_pars.Q / length;
			if ( length < min_length )
			{
				//~ std::cout << "[THREAD"<<std::this_thread::get_id()<<"] new min length found!\n";
				min_length = length;
				delete[] min_length_path;
				min_length_path = path;
				path = new float[n];
			}
		}
		synchronize_for_trail_merging_and_updating ( pheromone_trail, tm );
	}
	*min_path_ref = min_length_path;
	*min_length_ref = min_length;
}

int main ( const int argc, const char **argv )
{
	global_pars = parse_command_line_arguments ( argc, argv ) ;
	global_distances = read_graph ( *global_pars.input_file, global_pars.n );
	global_visibilities = distances_to_visibilities ( global_distances );
	int n = global_pars.n, m = global_pars.m;
	global_pheromone = new_empty_graph ( n );
	TrailToBeMergedConcurrently *tm = new TrailToBeMergedConcurrently;
	
	std::vector <std::thread> workers_handlers;
	int ants_number = global_pars.ants_number / m;
	int module = global_pars.ants_number % m;
	float **workers_min_paths = new float* [m];
	float *workers_paths_lengths = new float [m];
	
	//~ print_parameters ( global_pars );
	//~ print_graph ( global_distances, n );
	//~ std::cout << "[MAIN] init complete\n";
	
	for ( int i=0; i<m; ++i )
	{
		int ants_number_for_this_thread = ants_number + ( i < module ? 1 : 0 );
		std::thread th ( worker_routine, ants_number_for_this_thread, tm, &(workers_min_paths[i]), &(workers_paths_lengths[i]));
		workers_handlers.push_back ( std::move (th) );
	}
	//~ std::cout << "[MAIN] all threads spawned\n";
	if (global_pars.time_performances) global_ch.lap ( "init" );
	
	float min_length = std::numeric_limits<float>::infinity();;
	float *min_path = NULL;
	
	for ( int i=0; i<m; ++i )
	{
		workers_handlers[i].join();
		if ( workers_paths_lengths[i] < min_length )
		{
			min_length = workers_paths_lengths[i];
			delete [] min_path;
			min_path = workers_min_paths[i];
		}
		else
		{
			delete [] workers_min_paths[i];
		}
	}
	global_ch.restart ();
	//~ std::cout << "[MAIN] all threads joined\n";
		
	std::ofstream fout;
	fout.open ("tests_results/best_path_found");
	print_path ( min_path, n, fout );
	
	delete [] min_path;
	delete [] workers_min_paths;
	delete [] workers_paths_lengths;
	
	delete_graph ( global_pheromone, n );
	delete_graph ( global_distances, n );
	delete_graph ( global_visibilities, n );
	
	//~ std::cout << "[MAIN] shutdown complete. Goodbye\n";
	
	if (global_pars.time_performances) global_ch.lap ( "fin" );
	
	return 0;
}
