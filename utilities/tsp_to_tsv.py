
import math
import sys
from os.path import basename

def euclid_distance ( a, b ):
	return math.sqrt ( (a[0]-b[0])**2 + (a[1]-b[1])**2 )

if __name__ == "__main__":
	
	orig_file_name = basename (sys.argv[1])
	
	fin = open (sys.argv[1])
	inside = False
	coords = []
	
	for s_line in fin:
		line = s_line.strip()
		if line.startswith("NODE_COORD_SECTION") or line.startswith("DISPLAY_DATA_SECTION"):
			inside=True
		elif line.startswith("EOF"):
			if not inside:
				sys.stderr.write ("[ERROR] WITH FILE {}\n".format(sys.argv[1]))
				exit(-1)
			else:
				inside = False
		elif inside:
			_,x,y = line.split()
			coords.append ((float(x), float(y)))
	
	if len(coords)>500:
		sys.stderr.write ("Too big to convert: aborting\n")
		exit(0)
	
	fout = open ("../converted/"+orig_file_name+".tsv", "w")
	fout.write ("# {}\n".format(orig_file_name+".tsv"))
	fout.write ("# converted from original file: {}\n".format(orig_file_name))
	
	fout.write ("# DIMENSION : {}\n".format(len(coords)))
	fout.write ("#city1\tcity2\tdistance\n")
	
	for i in range ( len(coords) ):
		if i%100 == 0:
			print ("\r{:.2f}%".format(i/len(coords)*100),end="")
		for j in range ( i+1, len(coords) ):
			fout.write ("{}\t{}\t{:.4f}\n".format(i,j,euclid_distance(coords[i],coords[j])))
	print ("\r100%")
