
from aggregate_times import group_by_name
from collections import defaultdict
import subprocess as sp
import sys

import matplotlib.pyplot as plt
import math

_M_LIST = [ 1, 2, 4, 8, 16, 24, 32 ]
_S_LIST = [ 64, 256, 2048, 8192, 32768 ]
_FNAME_LIST = [ "ulysses22.tsp.tsv", "kroA200.tsp.tsv" ]

if __name__ == "__main__":

	dname = "input_examples"
	
	with open("/tmp/manual_tests.sh", "w") as fout:					   
		
		fout.write ( " ".join (["make", "clean", "all"]) + "\n" )
		
		for fname in _FNAME_LIST:
			
			input_file = dname+"/"+fname
			for s in _S_LIST:
				
				fout.write ( " ".join (["./aco_sequential", "-t", "-f", input_file, "-s", str(s)])+"\n" )
				fout.write ( " ".join (["cp", "tests_results/performances.tsv", "tests_results/"+fname+"_sequential_s"+str(s)+".row"] )+"\n" )
				#~ sp.run ( ["./aco_sequential", "-t", "-f", input_file, "-s", str(s)] )
				#~ sp.run ( ["mv", "tests_results/performances.tsv", "tests_results/"+fname+"_sequential_s"+str(s)+".row"] )
				
				for m in _M_LIST:
					
					fout.write ( " ".join (["./aco_parallel", "-t", "-f", input_file, "-s", str(s), "-m", str(m)])+"\n" )
					fout.write ( " ".join (["cp", "tests_results/performances.tsv", "tests_results/"+fname+"_parallel_s"+str(s)+"_m"+str(m)+".row"])+"\n" )
					#~ sp.run ( ["./aco_parallel", "-t", "-f", input_file, "-s", str(s), "-m", str(m)] )
					#~ sp.run ( ["mv", "tests_results/performances.tsv", "tests_results/"+fname+"_parallel_s"+str(s)+"_m"+str(m)+".row"] )
	
	sys.stderr.write ("RUNNING EXPERIMENTS ON SERVER\n")
	sp.run (["scp", "/tmp/manual_tests.sh", "spm18-messina@c6320p-2.itc.unipi.it:/home/spm18-messina/aco-tsp"], stdout=sys.stderr)
	sp.run (["ssh", "spm18-messina@c6320p-2.itc.unipi.it", 'cd aco-tsp; bash -x ./manual_tests.sh'], stdout=sys.stderr)
	sys.stderr.write ("COPYING RESULTS\n")
	sp.run (["scp", "-r", "spm18-messina@c6320p-2.itc.unipi.it:/home/spm18-messina/aco-tsp/tests_results/", "."], stdout=sys.stderr)
	
	sys.stderr.write ("COMPUTING TIMES\n")
	
	print (("{}\t"*16).format ("\\textbf{graph}", "\\textbf{s}", "\\textbf{m}", "$T_{pinit}$", "$T_{init}$",
							   "$T_1$", "$\\frac{s*T_{travel}}{m}$", "$T_2$", "$log(m) * T_{update}$",
							   "$T_3$", "$T_{update}$", "$T_{pfin}$", "$T_{fin}$",
							   "$T_{ptotal}$", "$\\frac{T_{total}}{m}$", "\\textbf{speedup}"))
	
	for fname in _FNAME_LIST:
		pretty_graph_name = fname.split(".")[0]
		series = defaultdict(list)
	
		input_file = dname+"/"+fname
		for s in _S_LIST:
			
			#~ print ( ["./aco_sequential", "-t", "-f", input_file, "-s", str(s)] )
			#~ print ( ["cp", "tests_results/performances.tsv", "tests_results/"+fname+"_sequential_s"+str(s)+".row"] )
			#~ sp.run ( ["./aco_sequential", "-t", "-f", input_file, "-s", str(s)] )
			#~ sp.run ( ["mv", "tests_results/performances.tsv", "tests_results/"+fname+"_sequential_s"+str(s)+".row"] )
			
			dic_seq = group_by_name ( open ("tests_results/"+fname+"_sequential_s"+str(s)+".row"), open ("tests_results/"+fname+"_sequential_s"+str(s)+".agg", "w") )
			
			for m in _M_LIST:
				
				#~ print ( ["./aco_parallel", "-t", "-f", input_file, "-s", str(s), "-m", str(m)] )
				#~ print ( ["cp", "tests_results/performances.tsv", "tests_results/"+fname+"_parallel_s"+str(s)+"_m"+str(m)+".row"] )
				#~ sp.run ( ["./aco_parallel", "-t", "-f", input_file, "-s", str(s), "-m", str(m)] )
				#~ sp.run ( ["mv", "tests_results/performances.tsv", "tests_results/"+fname+"_parallel_s"+str(s)+"_m"+str(m)+".row"] )
				
				dic_par = group_by_name ( open("tests_results/"+fname+"_parallel_s"+str(s)+"_m"+str(m)+".row"), open ("tests_results/"+fname+"_parallel_s"+str(s)+"_m"+str(m)+".agg", "w"))
				gain = dic_seq["total"]/dic_par["total"]
				print (("{}\t{}\t{}\t" + "{:.6g}\t"*13).format (
				                           pretty_graph_name, s, m, dic_par["init"], dic_seq["init"],
				                           dic_par["T1"], s*dic_seq["travel"]/m, dic_par["T2"], math.log(m,2)*dic_seq["update"],
				                           dic_par["T3"], dic_seq["update"], dic_par["fin"], dic_seq["fin"],
				                           dic_par["total"], dic_seq["total"]/m, gain) )
				
				
				series[s].append(gain)
	
		plt.plot ( _M_LIST, series[64],    "k.-", label='m=64' )
		plt.plot ( _M_LIST, series[256],   "ro-", label='m=256' )
		plt.plot ( _M_LIST, series[2048],  "c^-", label='m=2048' )
		plt.plot ( _M_LIST, series[8192],  "m*-", label='m=8192' )
		plt.plot ( _M_LIST, series[32768], "gs-", label='m=32768' )
		
		plt.legend( loc="best" )
		plt.title ("Scalabilities on graph {}".format(pretty_graph_name))
		plt.gca().set_xlabel ("parallelism degree (m)")
		plt.gca().set_ylabel ("speedup wrt sequential version")
		plt.savefig ("tests_results/scalabilities_{}.png".format(pretty_graph_name))
		plt.gca().clear()
		
