
import numpy as np
import sys
from collections import defaultdict

def group_by_name ( fin, fout ):
	total = 0
	dic = defaultdict (list)
	dic_out = {}
	
	# skip first line
	fin.readline()
	for line in fin:
		name,time = line.split()
		dic[name].append(float(time))
	
	for name in dic:
		avg = np.average( dic[name] )
		total += sum ( dic[name] )
		dic_out[name] = avg
		if len(dic[name]) > 1:
			stdev = np.std ( dic[name] )
			fout.write ("{}: {} +/- {}\n".format (name, avg, stdev) )
		else:
			fout.write ("{}: {}\n".format (name, avg) )
			
	fout.write ("total: {}\n".format (total) )
	dic_out["total"] = total
	return dic_out

if __name__ == "__main__":
	fin = sys.stdin
	if len(sys.argv) > 1:
		fin = open (sys.argv[1])
	group_by_name (fin, sys.stdout)
	
