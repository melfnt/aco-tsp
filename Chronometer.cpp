/**
 * Lucio Messina (matricola 505134)
 * 
 * Final project: ACO-TSP.
 * 
 * Chronometer.cpp - Implementaton of a class used to measure times.
 * For the documentaion, see Chronometer.hpp
 * 
 * */

#include "Chronometer.hpp"
#include <iostream>

Chronometer :: Chronometer ( std::string report_fname )
{
	this->fout.open ( report_fname );
	this->fout << "NAME\tTIME\n";
	this->started_time = std::chrono::high_resolution_clock::now();
}

void Chronometer :: lap ( std::string name )
{
	std::chrono::time_point<std::chrono::high_resolution_clock> ended_time = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = ended_time - this->started_time;
	this->fout << name << "\t" << diff.count() << "\n";
	this->started_time = std::chrono::high_resolution_clock::now();
}

void Chronometer :: restart ()
{
	this->started_time = std::chrono::high_resolution_clock::now();
}
